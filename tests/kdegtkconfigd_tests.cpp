#include <QtTest>
#include <QCoreApplication>

#include "kdegtkconfigd.h"

class KdeGtkConfigd_Tests : public QObject
{
    Q_OBJECT

public:
    KdeGtkConfigd_Tests();
    ~KdeGtkConfigd_Tests();

private Q_SLOTS:
    void changeGtkFont();
};

KdeGtkConfigd_Tests::KdeGtkConfigd_Tests()
{

}

KdeGtkConfigd_Tests::~KdeGtkConfigd_Tests()
{

}

void KdeGtkConfigd_Tests::changeGtkFont() {

}

QTEST_MAIN(KdeGtkConfigd_Tests)

#include "kdegtkconfigd_tests.moc"
