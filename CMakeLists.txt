cmake_minimum_required(VERSION 3.13)

project(KdeGtkConfigd VERSION 0.1)

add_executable(${PROJECT_NAME} "main.cpp")

add_subdirectory(src)
add_subdirectory(tests)

target_link_libraries(${PROJECT_NAME} PRIVATE libKdeGtkConfigd)
