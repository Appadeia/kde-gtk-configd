#include <QDebug>
#include <QPair>
#include <QDir>
#include <QFont>
#include <QtDBus/QDBusConnection>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QProcess>

#include <KSharedConfig>
#include <KConfigGroup>

#include <string>
#include <csignal>
#include <cstdio>

#include "kdegtkconfigd.h"
#include "gtkconfigdadaptor.h"

#undef signals
#include <gio/gio.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#define signals Q_SIGNALS


KdeGtkConfigd::KdeGtkConfigd(QObject *parent) : QObject(parent)
{
    qDebug() << "GTK configuration deamon is running...";
    new GtkConfigdAdaptor(this);
    QDBusConnection sessionBus = QDBusConnection::sessionBus();
    sessionBus.registerObject("/KdeGtkConfigd", this);
    sessionBus.registerService("org.kde.gtkConfigd");
}

void KdeGtkConfigd::changeGtkFont(const QString &fontName)
{
    changeGtk2Font(fontName);
    changeGtk3Font(fontName);
    reloadGtk2Apps();
    reloadGtk3Apps();
    qDebug() << "Font changed to " << fontName;
}

void KdeGtkConfigd::changeGtk2Font(const QString &fontName)
{
    modifyGtk2ConfigValue("gtk-font-name", fontName);
}

void KdeGtkConfigd::changeGtk3Font(const QString &fontName)
{
    changeGtk3ConfigValueWayland("font-name", fontName);
    changeGtk3ConfigValueX11SettingsIni("gtk-font-name", fontName);
    changeGtk3ConfigValueX11XSettingsd("Gtk/FontName", fontName);
}

void KdeGtkConfigd::changeGtk3ConfigValueWayland(const QString &paramName, const QString &paramValue)
{
    gtk_init(nullptr, nullptr);
    g_autoptr(GSettings) gsettings = g_settings_new("org.gnome.desktop.interface");
    g_settings_set_string(gsettings, paramName.toUtf8().constData(), paramValue.toUtf8().constData());
}

void KdeGtkConfigd::changeGtk3ConfigValueX11SettingsIni(const QString &paramName, const QString &paramValue)
{
    using qsp = QStandardPaths;
    QString configLocation = qsp::writableLocation(qsp::GenericConfigLocation);
    QString gtk3ConfigPath{configLocation + "/gtk-3.0/settings.ini"};

    KSharedConfig::Ptr gtk3Config = KSharedConfig::openConfig(gtk3ConfigPath, KConfig::NoGlobals);
    KConfigGroup group{gtk3Config, "Settings"};

    group.writeEntry(paramName, paramValue);
    group.sync();
}

void KdeGtkConfigd::changeGtk3ConfigValueX11XSettingsd(const QPair<QString, QString> &paramKeyValuePair)
{
    QString xSettingsdConfigPath {QDir::homePath() + "/.config/xsettingsd/xsettingsd.conf"};
    QFile xSettingsdConfig {xSettingsdConfigPath};
    QString xSettingsdConfigContents {readFileContents(xSettingsdConfig)};
    replaceValueInXSettingsdContents(xSettingsdConfigContents, paramKeyValuePair);
    xSettingsdConfig.remove();
    xSettingsdConfig.open(QIODevice::WriteOnly | QIODevice::Text);
    xSettingsdConfig.write(xSettingsdConfigContents.toUtf8());
}

void KdeGtkConfigd::changeGtk3ConfigValueX11XSettingsd(const QString &paramName, const QString &paramValue)
{
    QPair<QString, QString> paramKeyValuePair {paramName, paramValue};
    changeGtk3ConfigValueX11XSettingsd(paramKeyValuePair);
}

void KdeGtkConfigd::modifyGtk2ConfigValue(const QString &paramName, const QString &paramValue)
{
    QPair<QString, QString> paramKeyValuePair {paramName, paramValue};
    modifyGtk2ConfigValue(paramKeyValuePair);
}

void KdeGtkConfigd::modifyGtk2ConfigValue(const QPair<QString, QString> &paramKeyValuePair)
{
    QString gtkrcPath {QDir::homePath() + "/.gtkrc-2.0"};
    QFile gtkrc {gtkrcPath};
    QString gtkrcContents {readFileContents(gtkrc)};
    replaceValueInGtkrcContents(gtkrcContents, paramKeyValuePair);
    gtkrc.remove();
    gtkrc.open(QIODevice::WriteOnly | QIODevice::Text);
    gtkrc.write(gtkrcContents.toUtf8());
}

QString KdeGtkConfigd::readFileContents(QFile &file)
{
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return file.readAll();
    } else {
        qWarning() << "Unable to read the " << file.fileName() << " file";
        return "";
    }
}


void KdeGtkConfigd::replaceValueInGtkrcContents(QString &gtkrcContents, const QPair<QString, QString> &paramKeyValuePair)
{
    QString paramName {paramKeyValuePair.first};
    QString paramValue {paramKeyValuePair.second};
    QRegularExpression regExp {paramName + "=[^\n]*($|\n)"};

    static const QStringList nonStringProperties{
        "gtk-toolbar-style",
        "gtk-menu-images",
        "gtk-button-images",
        "gtk-primary-button-warps-slider",
    };

    QString newConfigString;
    if (nonStringProperties.contains(paramName)) {
        newConfigString = paramName + "=" + paramValue + "\n";
    } else {
        newConfigString = paramName + "=\"" + paramValue + "\"\n";
    }

    if (gtkrcContents.contains(regExp)) {
        gtkrcContents.replace(regExp, newConfigString);
    } else {
        gtkrcContents = newConfigString + "\n" + gtkrcContents;
    }
}

void KdeGtkConfigd::replaceValueInXSettingsdContents(QString &xSettingsdContents, const QPair<QString, QString> &paramKeyValuePair)
{
    QString paramName {paramKeyValuePair.first};
    QString paramValue {paramKeyValuePair.second};
    QRegularExpression regExp {paramName + " [^\n]*($|\n)"};
    QString newConfigString {paramName + " \"" + paramValue + "\"\n"};

    if (xSettingsdContents.contains(regExp)) {
        xSettingsdContents.replace(regExp, newConfigString);
    } else {
        xSettingsdContents = newConfigString + "\n" + xSettingsdContents;
    }
}

void KdeGtkConfigd::reloadGtk2Apps()
{
    QProcess::startDetached(QStandardPaths::findExecutable("gtk2apps_reloader"));
}

void KdeGtkConfigd::reloadGtk3Apps()
{
    pid_t pidOfXSettingsd = getPidOfXSettingsd();
    qDebug() << "Pid of XSettings: " << pidOfXSettingsd;
    if (pidOfXSettingsd == 0) {
        qDebug() << "XSettingsd does not exist. Starting new instance...";
        QProcess::startDetached(QStandardPaths::findExecutable("xsettingsd"));
    } else {
        qDebug() << "XSettingsd exists. Sending signal...";
//        system("killall -HUP xsettingsd");
        kill(pidOfXSettingsd, SIGHUP);
    }
}

pid_t KdeGtkConfigd::getPidOfXSettingsd()
{
    char line[512];
    FILE *cmd = popen("pidof -s xsettingsd", "r");
    fgets(line, 512, cmd);
    pclose(cmd);
    return std::atoi(line);
}
