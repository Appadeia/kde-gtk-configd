#ifndef KDEGTKCONFIGD_H
#define KDEGTKCONFIGD_H

#include <QObject>

class QFile;

class KdeGtkConfigd : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.kde.gtkConfigd")
public:
    explicit KdeGtkConfigd(QObject *parent = nullptr);

Q_SIGNALS:

public Q_SLOTS:
    void changeGtkFont(const QString &fontName);

private:
    void changeGtk2Font(const QString &fontName);
    void changeGtk3Font(const QString &fontName);

    void modifyGtk2ConfigValue(const QString &paramName, const QString &paramValue);
    void modifyGtk2ConfigValue(const QPair<QString, QString> &paramKeyValuePair);

    void changeGtk3ConfigValueWayland(const QString &paramName, const QString &paramValue);
    void changeGtk3ConfigValueX11SettingsIni(const QString &paramName, const QString &paramValue);

    void changeGtk3ConfigValueX11XSettingsd(const QString &paramName, const QString &paramValue);
    void changeGtk3ConfigValueX11XSettingsd(const QPair<QString, QString> &paramKeyValuePair);

    QString readFileContents(QFile &gtkrc);
    void replaceValueInGtkrcContents(QString &gtkrcContents, const QPair<QString, QString> &paramKeyValuePair);
    void replaceValueInXSettingsdContents(QString &xSettingsdContents, const QPair<QString, QString> &paramKeyValuePair);

    void reloadGtk2Apps();
    void reloadGtk3Apps();

    pid_t getPidOfXSettingsd();
};

#endif // KDEGTKCONFIGD_H
