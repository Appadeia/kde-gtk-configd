#include <stdio.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>

int main(int argc, char** argv) {
    gtk_init(&argc, &argv);

    GdkEventClient event;
    event.type = GDK_CLIENT_EVENT;
    event.send_event = TRUE;
    event.window = NULL;
    event.message_type = gdk_atom_intern("_GTK_READ_RCFILES", FALSE);
    event.data_format = 8;

    gdk_event_send_clientmessage_toall((GdkEvent *)&event);
    return 0;
}
