#include <QCoreApplication>
#include "kdegtkconfigd.h"

int main(int argc, char *argv[])
{
    QCoreApplication a{argc, argv};

    KdeGtkConfigd daemon{};

    return a.exec();
}
